# Ecommerce

This is a challenge the Factor-IT


## Comenzando

### Pre-requisitos
_Para este proyecto se utilizo MySql Workbench , por lo que es necesario tenerlo instalado: https://www.mysql.com/products/workbench/
_PostMan para poder realizar pruebas de backend

### Pruebas backend
_En total hay 7 endpoint { 2 GET, 2 PUT, 2 DELETE y 1 POST } los mismos estan especificados en el swagger, el mismo esta en formato HTML y yaml para poder utilizar con https://editor.swagger.io/
_No se realizaron test unitarios por falta de tiempo
	
### Scheduler:
_Se realizaron dos scheduler:
_Uno se ejecuta cada 30 minutos y es para verificar el estado del carrito de compra. Si la ultima actualizacion supero los 30 minutos, dicho carrito sera eliminado.
_Otro scheduler se ejecuta cada 15 minutos, y es para que a principio de cada mes los usuarios puedan volver a ser considerados VIP. 
