package com.factorit.ecommerce.controller;

import com.factorit.ecommerce.entity.Person;
import com.factorit.ecommerce.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/persons")
public class PersonController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    PersonRepository personRepository;

    @PostMapping("/new")
    Person createPerson(@Valid @RequestBody Person person){

        LOGGER.info("Request body to create a new person: {}" , person);

        return personRepository.save(person);
    }


}
