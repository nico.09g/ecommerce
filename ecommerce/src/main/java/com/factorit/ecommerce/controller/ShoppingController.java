package com.factorit.ecommerce.controller;

import com.factorit.ecommerce.entity.Product;
import com.factorit.ecommerce.entity.Shop;
import com.factorit.ecommerce.entity.ShopDetail;
import com.factorit.ecommerce.repository.DetailRepository;
import com.factorit.ecommerce.repository.PersonRepository;
import com.factorit.ecommerce.repository.ShopRepository;
import com.factorit.ecommerce.service.ShoppingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/shop")
public class ShoppingController {

    @Autowired
    ShopRepository shopRepository;

    @Autowired
    DetailRepository detailRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ShoppingService service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @PostMapping("car")
    ResponseEntity<?> createCar(@Valid @RequestBody Shop shoppingCar) {

        LOGGER.info("Request to create a new shopping car {}", shoppingCar);
        return service.createNewShopCar(shoppingCar);
    }

    @GetMapping("cars/{id}")
    ResponseEntity<?> getShoppingCar(@PathVariable Long id) {
        LOGGER.info("Request to search the next id shop car: {} ", id);
        return service.getOneShopCar(id);
    }

    @PutMapping("car")
    ResponseEntity<?> addProduct(@Valid @RequestBody ShopDetail detail) {
        LOGGER.info("Request to add a new product: {}", detail);
        return service.addNewProduct(detail);
    }

    @DeleteMapping("car/product/{id}")
    ResponseEntity<?> deleteProduct(@PathVariable Long id, @Valid @RequestBody Product product) {

        LOGGER.info("Request to delete a product: {}, in the car shop: {}", product, id);
        return service.deleteProduct(id, product);

    }

    @DeleteMapping("cars/shop/{id}")
    ResponseEntity<?> deleteShoppingCar(@PathVariable Long id) {
        LOGGER.info("Request to delete car shop: {}", id);
        return service.deleteShopCar(id);
    }

    @GetMapping("car/total/{id}")
    ResponseEntity<?> getTotalToPay(@PathVariable Long id) {

        LOGGER.info("Request to get information to pay a car shop: {}", id);
        return service.getToPayShopCar(id);


    }

    @PutMapping("car/payment/{id}")
    ResponseEntity<?> payCarShop(@PathVariable final Long id) {

        LOGGER.info("Request to pay a car shop: {}", id);
        return service.payShopCat(id);

    }


}
