package com.factorit.ecommerce.controller;


import com.factorit.ecommerce.entity.Product;
import com.factorit.ecommerce.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @PostMapping("product")
    Product createProduct(@Valid @RequestBody Product product){

        LOGGER.info("Request to create a new product {}", product);

        return productRepository.save(product);

    }

    @GetMapping("product")
    List<Product> getProducts(){
        return productRepository.findAll();
    }

}
