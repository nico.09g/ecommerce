package com.factorit.ecommerce.repository;

import com.factorit.ecommerce.entity.ShopDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailRepository extends JpaRepository<ShopDetail, Long> {


}
