package com.factorit.ecommerce.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "shop")
public class Shop {

    public Shop() {
    }

    public Shop(String state, Set<Product> products, Person person) {
        this.state = state;
        this.person = person;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String state;

    private Boolean discount;

    @OneToMany(mappedBy = "shop")
    Set<ShopDetail> shopDetails;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private Person person;

    private String hourUpdate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getHourUpdate() {
        return hourUpdate;
    }

    public void setHourUpdate(String hourUpdate) {
        this.hourUpdate = hourUpdate;
    }

    public Set<ShopDetail> getShopDetails() {
        return shopDetails;
    }

    public void setShopDetails(Set<ShopDetail> shopDetails) {
        this.shopDetails = shopDetails;
    }

    public Boolean getDiscount() {
        return discount;
    }

    public void setDiscount(Boolean discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "id=" + id +
                ", state='" + state + '\'' +
                ", hourUpdate='" + hourUpdate + '\'' +
                '}';
    }
}
