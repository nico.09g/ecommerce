package com.factorit.ecommerce.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {

    public Product() {
    }

    public Product(String name, Long price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Long price;

    private String description;

    @OneToMany(mappedBy = "product")
    Set<ShopDetail> shopDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ShopDetail> getShopDetails() {
        return shopDetails;
    }

    public void setShopDetails(Set<ShopDetail> shopDetails) {
        this.shopDetails = shopDetails;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
