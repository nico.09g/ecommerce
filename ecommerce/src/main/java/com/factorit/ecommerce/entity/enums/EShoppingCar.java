package com.factorit.ecommerce.entity.enums;

public enum EShoppingCar {

    PAYMENT,
    CANCELLED,
    IN_PROCESS;

}
