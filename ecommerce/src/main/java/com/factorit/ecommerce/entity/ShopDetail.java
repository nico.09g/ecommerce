package com.factorit.ecommerce.entity;

import javax.persistence.*;

@Entity
public class ShopDetail {

    public ShopDetail() {
    }

    public ShopDetail(Shop shop, Product product, int count) {
        this.shop = shop;
        this.product = product;
        this.count = count;
    }

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    Long Id;

    @ManyToOne
    @JoinColumn(name = "shop_id")
    Shop shop;

    @ManyToOne
    @JoinColumn(name = "product_id")
    Product product;

    int count;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "ShopDetail{" +
                "Id=" + Id +
                ", shop=" + shop +
                ", product=" + product +
                ", count=" + count +
                '}';
    }
}
