package com.factorit.ecommerce.service.scheduler;

import com.factorit.ecommerce.entity.Person;
import com.factorit.ecommerce.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class ScheduledStateVIP {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledStateVIP.class);
    private static final String PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    @Autowired
    PersonRepository personRepository;

    @Async
    @Scheduled(fixedRate = 900000)
    public void scheduledStateVIP() throws ParseException {


        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);


        if (calendar.getTime().getDay() == 0 && calendar.getTime().getHours() == 00) {
            List<Person> personList = personRepository.findAll();

            for (Person person : personList) {
                person.setVIP("N");
                personRepository.save(person);
            }
        }
    }


}
