package com.factorit.ecommerce.service.scheduler;

import com.factorit.ecommerce.entity.Shop;
import com.factorit.ecommerce.entity.ShopDetail;
import com.factorit.ecommerce.entity.enums.EShoppingCar;
import com.factorit.ecommerce.repository.DetailRepository;
import com.factorit.ecommerce.repository.ShopRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class ScheduledShop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledShop.class);
    private static final String PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    @Autowired
    ShopRepository shopRepository;

    @Autowired
    DetailRepository detailRepository;

    @Async
    @Scheduled(fixedRate = 1800000)
    public void scheduledFixRateTask() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATTERN);
        String strHour = simpleDateFormat.format(new Date());

        List<Shop> shopList = shopRepository.findAll();

        try {
            for (Shop shop : shopList) {
                if (shop.getState().equals(EShoppingCar.IN_PROCESS)) {
                    Date shopDate = new SimpleDateFormat(PATTERN).parse(shop.getHourUpdate());
                    Date timeNow = new SimpleDateFormat(PATTERN).parse(strHour);

                    long minutes = TimeUnit.MILLISECONDS.toMinutes(shopDate.getTime() - timeNow.getTime());

                    if (minutes > 30) {
                        LOGGER.info("The shop car {} is beginning to delete", shop);
                        cancellShop(shop);
                        LOGGER.info("The shop car its deleted");
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void cancellShop(Shop shop) {

        for (ShopDetail shopDetail : shop.getShopDetails()) {
            detailRepository.delete(shopDetail);
        }

        shopRepository.delete(shop);
    }

}
