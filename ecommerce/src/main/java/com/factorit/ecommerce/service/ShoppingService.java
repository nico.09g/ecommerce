package com.factorit.ecommerce.service;

import com.factorit.ecommerce.Response.MessageResponse;
import com.factorit.ecommerce.Response.enums.EResult;
import com.factorit.ecommerce.entity.Person;
import com.factorit.ecommerce.entity.Product;
import com.factorit.ecommerce.entity.Shop;
import com.factorit.ecommerce.entity.ShopDetail;
import com.factorit.ecommerce.entity.enums.EShoppingCar;
import com.factorit.ecommerce.repository.DetailRepository;
import com.factorit.ecommerce.repository.PersonRepository;
import com.factorit.ecommerce.repository.ShopRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ShoppingService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ShopRepository shopRepository;

    @Autowired
    DetailRepository detailRepository;

    @Value("#{'${list.days.discount}'.split(',')}")
    List<String> listDiscount;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private Calendar calendar = Calendar.getInstance();

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingService.class);

    private LocalDateTime localDateTime = LocalDateTime.now();

    public ResponseEntity<?> createNewShopCar(Shop shoppingCar) {

        LocalDateTime localDateTime = LocalDateTime.now();

        shoppingCar.setState(EShoppingCar.IN_PROCESS.name());
        shoppingCar.setHourUpdate(localDateTime.toString());

        return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                shopRepository.save(shoppingCar).toString()));

    }

    public ResponseEntity<?> getOneShopCar(Long id) {

        Optional<Shop> shopOptional = shopRepository.findById(id);

        if (shopOptional.isPresent()) {
            return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                    shopOptional.get().getShopDetails().toString()));

        } else {
            LOGGER.warn("Invalid id shop car: {} " + id);

            return ResponseEntity.badRequest().body
                    (new MessageResponse(EResult.ERROR.getData(), "INVALID ID: " + id));
        }

    }

    public ResponseEntity<?> addNewProduct(ShopDetail detail) {

        Optional<Shop> shop = shopRepository.findById(detail.getShop().getId());

        if (shop.isPresent()) {

            LOGGER.info("Shopping car, update hour: {}", localDateTime);

            shop.get().setHourUpdate(localDateTime.toString());
            shopRepository.save(shop.get());
            detailRepository.save(detail);

            return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                    "The product was added in the shop car"));
        } else {
            LOGGER.warn("Invalid id shop car: {} ", detail.getShop().getId());

            return ResponseEntity.badRequest().body
                    (new MessageResponse(EResult.ERROR.getData(),
                            "INVALID SHOP CAR ID: " + detail.getShop().getId()));
        }
    }

    public ResponseEntity<?> deleteProduct(Long id, Product product) {

        Optional<Shop> shop = shopRepository.findById(id);

        if (shop.isPresent()) {
            for (ShopDetail shopDetail : shop.get().getShopDetails()) {
                if (shopDetail.getProduct().getId() == product.getId()) {
                    detailRepository.delete(shopDetail);
                    return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                            "The product was deleted in the shop car"));
                } else {

                    LOGGER.warn("Invalid product id: {} ", product.getId());

                    return ResponseEntity.badRequest().body
                            (new MessageResponse(EResult.ERROR.getData(),
                                    "INVALID PRODUCT ID: " + product.getId()));
                }
            }
        } else {

            LOGGER.warn("Invalid id shop car: {} ", id);

            return ResponseEntity.badRequest().body
                    (new MessageResponse(EResult.ERROR.getData(),
                            "INVALID SHOP CAR ID: " + id));
        }

        return ResponseEntity.badRequest().body
                (new MessageResponse(EResult.ERROR.getData(),
                        "INVALID SHOP CAR ID: " + id));
    }

    public ResponseEntity<?> deleteShopCar(Long id) {
        Optional<Shop> shop = shopRepository.findById(id);

        if (shop.isPresent()) {
            for (ShopDetail shopDetail : shop.get().getShopDetails()) {
                detailRepository.delete(shopDetail);
            }
            shopRepository.deleteById(id);
            return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                    "The shop car was deleted"));

        } else {
            return ResponseEntity.badRequest().body
                    (new MessageResponse(EResult.ERROR.getData(),
                            "INVALID SHOP CAR ID: " + id));
        }

    }

    public ResponseEntity<?> getToPayShopCar(Long id) {

        long subTotal = 0;
        long total = 0;

        Optional<Shop> shop = shopRepository.findById(id);

        try {
            if (shop.isPresent()) {
                int totalCountProduct = 0;
                for (ShopDetail shopDetail : shop.get().getShopDetails()) {
                    if (shop.get().getState().equals(EShoppingCar.IN_PROCESS.name())) {
                        int countProduct = 0;

                        LOGGER.info("shopping car state - product: {} ",
                                shopDetail.getProduct().getName());

                        LOGGER.info("DETAIL:");
                        LOGGER.info("CANT: {} , UNIT PRICE: {}", shopDetail.getCount(), shopDetail.getProduct().getPrice());
                        LOGGER.info("TO PAY: {} ", (shopDetail.getCount() * shopDetail.getProduct().getPrice()));

                        totalCountProduct += shopDetail.getCount();
                        countProduct += shopDetail.getCount();

                        if (countProduct > 4) {
                            double cont = countProduct / 4;
                            totalCountProduct -= Math.floor(cont);
                        }
                        subTotal += shopDetail.getCount() * shopDetail.getProduct().getPrice();
                    }
                }

                boolean itsVIP = determinateVIP(shop.get().getPerson().getId());

                total = determinateDiscount(subTotal, totalCountProduct,
                        shop.get().getHourUpdate(), itsVIP, shop.get().getPerson());
            } else {
                return ResponseEntity.badRequest().body
                        (new MessageResponse(EResult.ERROR.getData(),
                                "INVALID SHOP CAR ID: " + id));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        LOGGER.info("Total to pay: {}", subTotal);

        return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                "You moust be pay :" + total));
    }

    public ResponseEntity<?> payShopCat(Long id) {
        Optional<Shop> shop = shopRepository.findById(id);

        if (shop.isPresent()) {
            shop.get().setState(EShoppingCar.PAYMENT.name());
            shopRepository.save(shop.get());
            return ResponseEntity.ok().body(new MessageResponse(EResult.SUCCESSFUL.getData(),
                    "The shop car was payed"));
        } else {
            return ResponseEntity.badRequest().body
                    (new MessageResponse(EResult.ERROR.getData(),
                            "INVALID SHOP CAR ID: " + id));
        }
    }

    boolean determinateVIP(Long id) throws ParseException {
        Optional<Person> person = personRepository.findById(id);

        Set<Shop> listShop = person.get().getCarList();
        long countPaymentMonth = 0;

        for (Shop shop : listShop) {
            String strHour = simpleDateFormat.format(new Date());
            Date shopDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(shop.getHourUpdate());
            Date dateNow = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(strHour);

            calendar.setTime(shopDate);
            int shopCalendar = calendar.get(Calendar.MONTH);

            calendar.setTime(dateNow);
            int shopNow = calendar.get(Calendar.MONTH);

            if (shopDate.before(dateNow) && shop.getState().equals(EShoppingCar.PAYMENT.name()) && shopCalendar == shopNow) {
                for (ShopDetail shopDetail : shop.getShopDetails()) {
                    LOGGER.info("shopping car state - product: {} ",
                            shopDetail.getProduct().getName());

                    LOGGER.info("DETAIL:");
                    LOGGER.info("CANT: {} , UNIT PRICE: {}", shopDetail.getCount(), shopDetail.getProduct().getPrice());
                    LOGGER.info("TO PAY: {} ", (shopDetail.getCount() * shopDetail.getProduct().getPrice()));

                    countPaymentMonth += shopDetail.getCount() * shopDetail.getProduct().getPrice();
                }


            }
        }

        if (countPaymentMonth > 10000 && person.get().getVIP() != null && !person.get().getVIP().equals("U")) {
            return true;
        }

        return false;
    }

    public long determinateDiscount(long total, int countProduct,
                                    String hourUpdate, boolean itsVIP, Person person) {

        String[] dateParse = hourUpdate.split("T");

        if (itsVIP && total > 5000) {
            total -= 2000;
            person.setVIP("U");
            personRepository.save(person);
        }

        boolean apply = false;
        if (countProduct > 10) {
            for (String date : listDiscount) {
                if (date.equals(dateParse[0])) {
                    total -= 500;
                    apply = true;
                    break;
                }
            }
            if (!apply) {
                total -= (countProduct * 100);
            }
        }

        return total;
    }

}
