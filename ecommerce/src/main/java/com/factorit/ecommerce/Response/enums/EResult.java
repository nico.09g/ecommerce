package com.factorit.ecommerce.Response.enums;

public enum EResult {
    SUCCESSFUL("Operation successful!"),
    ERROR("An error ocurrer in the server!");

    private String data;

    EResult(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
